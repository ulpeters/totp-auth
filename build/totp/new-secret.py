import pyotp, pyqrcode, os

# Get some addition data for the QR not used for TOTP
#issuer = input("Issuer: ")
#email = input("E-Mail: ")

issuer = '' 
email  = '' 

# Generate random secret and URL for the QR code
TOTP_SECRET=pyotp.random_base32()
TOTP_URL=pyotp.totp.TOTP(TOTP_SECRET).provisioning_uri(email, issuer_name=issuer)

# Print Secret and QR code
#print(TOTP_SECRET)
#print(pyqrcode.create(TOTP_URL).terminal(quiet_zone=1))

env = open('/mnt/.env', 'w')
env.write("SECRET=" + TOTP_SECRET)
env.close()

qr = open('/mnt/secret.qr', 'w')
qr.write(pyqrcode.create(TOTP_URL).terminal(quiet_zone=1))
qr.close()
