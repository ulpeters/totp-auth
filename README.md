# Simple totp subrequest authentication

To be used with [reverse-proxy](https://github.com/nginx-proxy/nginx-proxy)

## Setup
```
# build the container image
docker compose build

# Popoulate .env file with an random secret and a corresponding qr-code
# for your TOTP mobile app stored in secret.qr`
./new-secret.sh

# Copy and adapt sample configuration
cp vdi.example.com_location /opt/docker/reverse-proxy/data/vhost.d/ts.lime.s-up.net_location
```

## Credits:
  - Based on [newhouseb/nginxwebauthn](https://github.com/newhouseb/nginxwebauthn)

## References:
  - [NGINX: Authentication Based on Subrequest Result](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-subrequest-authentication/)
